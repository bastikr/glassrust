#![feature(test)]

extern crate bmp;
use bmp::Image;

//use benchmark;
pub mod geometry;
use geometry::{FACES, Face, intersect_face, UnitVector3d, Vector3d, Ray, Position};





const LIGHTDIMENSIONS: usize = 3;
type Light = [f64; LIGHTDIMENSIONS];


fn medium(l0: Light, length:f64, gamma: Light, omega: Light) -> Light {
    let mut l1: Light = [0.; LIGHTDIMENSIONS];
    for i in 0..LIGHTDIMENSIONS {
        let alpha = (-gamma[i]*length).exp();
        l1[i] = l0[i]*alpha + omega[i]*(1.-alpha);
    }
    l1
}

fn reflectance(n1: f64, n2: f64, face: &Face, direction: UnitVector3d) -> f64{
    let cosalpha = face.dot_unitvector(direction).abs();
    if n1==n2 {
        return 0.
    } else if n1>n2 && (1.-(n2/n1).powi(2)) >= cosalpha.powi(2) {
        return 1.
    }
    let cosbeta = (1. - (n1/n2).powi(2)*(1.-cosalpha.powi(2))).sqrt();
    let rs = (n1*cosalpha - n2*cosbeta)/(n1*cosalpha + n2*cosbeta);
    let rp = (n2*cosalpha - n1*cosbeta)/(n2*cosalpha + n1*cosbeta);
    return 0.5*rs.powi(2) + 0.5*rp.powi(2)
}

fn intersect_brick(brick: &Brick, ray: &Ray, excluded_face: Option<&Face>) -> Option<(f64, Vector3d, &'static Face)> {
    //let p0 = brick.position.vector3d();
    //let p1 = brick.position.vector3d() + Vector3d::new(1.,1.,1.);
    let mut intersection = None::<(f64, Vector3d, &Face)>;
    for face in FACES.iter() {
        if excluded_face.is_some() && excluded_face.unwrap()==face {
            continue
        }
        let result = intersect_face(brick.position, face, ray);
        // let result = if face.normal.positive {
        //     intersect_face(p1, face, ray)
        // } else {
        //     intersect_face(p0, face, ray)
        // };
        if result.is_some() && (intersection.is_none() || result.unwrap().0 < intersection.unwrap().0) {
            intersection = Some((result.unwrap().0, result.unwrap().1, face));
        }
    }
    intersection
}

fn intersect_world<'a>(world: &'a World, ray: &Ray, excluded_brick: Option<&Brick>) -> Option<(f64, Vector3d, &'a Brick, &'static Face)> {
    let mut intersection = None::<(f64, Vector3d, &Brick, &Face)>;
    for brick in world.bricks.iter() {
        if excluded_brick.is_some() && excluded_brick.unwrap().position==brick.position {
            continue
        }
        let result = intersect_brick(brick, ray, None::<&Face>);
        if result.is_some() && (intersection.is_none() || result.unwrap().0<intersection.unwrap().0) {
            intersection = Some((result.unwrap().0, result.unwrap().1, brick, result.unwrap().2));
        }
    }
    intersection
}


fn propagate_medium(world: &World, ray: &Ray, brick: &Brick, excluded_face: Option<&Face>) -> Light {
    if let Some((d, p, face)) = intersect_brick(brick, ray, excluded_face) {
        let reflection = reflectance(brick.n, world.n, face, ray.direction);
        //let R = 0.1;
        let ray_leaving = Ray{origin: p, direction: ray.direction, reflections: ray.reflections, importance: (1.-reflection)*ray.importance};
        let l_leaving = propagate_free(world, &ray_leaving, Some(brick));
        if ray.reflections > world.max_reflections {
            return l_leaving
        }
        let ray_reflected = Ray{origin: p, direction: ray.direction.reflect(face), reflections: ray.reflections+1, importance: reflection*ray.importance};
        let l_reflected = propagate_medium(world, &ray_reflected, brick, Some(face));
        let mut l: Light = [0.; LIGHTDIMENSIONS];
        for i in 0..LIGHTDIMENSIONS {
            l[i] = reflection*l_reflected[i] + (1.-reflection)*l_leaving[i];
        }
        medium(l, d, brick.gamma, brick.omega)
    } else {
        unreachable!()
    }
}

fn propagate_free(world: &World, ray: &Ray, excluded_brick: Option<&Brick>) -> Light {
    let intersection = intersect_world(world, ray, excluded_brick);
    if let Some((_, p, brick, face)) = intersection {
        let reflection = reflectance(world.n, brick.n, face, ray.direction);
        let ray_entering = Ray{origin: p, direction: ray.direction, reflections: ray.reflections, importance: (1.-reflection)*ray.importance};
        let l_entering = propagate_medium(world, &ray_entering, brick, Some(face));
        if ray.reflections > world.max_reflections {
            return l_entering
        }
        let ray_reflected = Ray{origin: p, direction: ray.direction.reflect(face), reflections: ray.reflections+1, importance: reflection*ray.importance};
        let l_reflected = propagate_free(world, &ray_reflected, Some(brick));
        let mut l: Light = [0.; LIGHTDIMENSIONS];
        for i in 0..LIGHTDIMENSIONS {
            l[i] = reflection*l_reflected[i] + (1.-reflection)*l_entering[i];
        }
        return l
    } else {
        return [0.8;3]
    }
}

fn render(world: &World, camera: &Camera) {
    let mut img = Image::new(camera.resolution_x as u32, camera.resolution_y as u32);
    let mut intensity_map : Vec<Light> = Vec::with_capacity(camera.resolution_x*camera.resolution_y);
    let mut max_val = 0.;

    for ix in 0..camera.resolution_x {
        for iy in 0..camera.resolution_y {
            let ex = camera.horizontal;
            let ey = ex.cross(camera.direction);
            let fx = ((ix as f64)/((camera.resolution_x-1) as f64) - 0.5)*camera.width_x;
            let fy = ((iy as f64)/((camera.resolution_y-1) as f64) - 0.5)*camera.width_y;
            let direction = (camera.direction.scale(camera.focallength) + ex.scale(fx) + ey.scale(fy)).unitvector();
            let ray = Ray{origin: camera.position, direction: direction, reflections: 0, importance: 1.};

            let c = propagate_free(&world, &ray, None::<&Brick>);
            for val in c.iter() {
                if *val > max_val {
                    max_val = *val;
                }
            }
            intensity_map.push(c);
        }
    }
    for ix in 0..camera.resolution_x {
        for iy in 0..camera.resolution_y {
            let c = intensity_map[ix*camera.resolution_y+iy];
            img.set_pixel(ix as u32, iy as u32, bmp::Pixel { r: (c[0]*255./max_val) as u8, g: (c[1]*255./max_val) as u8, b: (c[2]*255./max_val) as u8 });
        }
    }
    img.save("t.bmp");
}

struct Brick {
    pub position: Position,
    pub n: f64,
    pub gamma: Light,
    pub omega: Light,
}

struct Camera {
    pub position: Vector3d,
    pub direction: UnitVector3d,
    pub horizontal: UnitVector3d,
    pub focallength: f64,
    pub width_x: f64,
    pub width_y: f64,
    pub resolution_x: usize,
    pub resolution_y: usize
}

struct World {
    // background: Color,
    bricks: Vec<Brick>,
    n: f64,
    max_reflections: usize,
}



fn main() {
    let b1 = Brick{
        position: Position::new(0, 0, 4),
        n: 1.3,
        gamma: [0.5, 0.5, 0.5],
        omega: [0.3, 0.9, 0.],
    };
    let b2 = Brick{
        position: Position::new(0, 0, 6),
        n: 1.3,
        gamma: [0.5, 0.5, 0.5],
        omega: [0., 1., 0.],
    };
    let b3 = Brick{
        position: Position::new(0, 0, 8),
        n: 1.3,
        gamma: [0.8, 0.8, 0.8],
        omega: [0., 0.9, 0.9],
    };
    let b4 = Brick{
        position: Position::new(0, -2, 4),
        n: 1.3,
        gamma: [0.5, 0.5, 0.],
        omega: [1., 0., 0.],
    };
    let b5 = Brick{
        position: Position::new(0, -2, 6),
        n: 1.3,
        gamma: [0.5, 0.5, 0.5],
        omega: [0., 1., 0.],
    };
    let b6 = Brick{
        position: Position::new(0, -2, 8),
        n: 1.3,
        gamma: [0.4, 0.4, 0.4],
        omega: [0.9, 0.1, 0.1],
    };
    let camera = Camera{
        position: Vector3d::new(3., 4., 0.),
        direction: UnitVector3d::new(-0.3, -1., 1.),
        horizontal: UnitVector3d::new(1., 0., 0.),
        focallength: 1.,
        width_x: 1.,
        width_y: 3./4.,
        resolution_x: 1200,
        resolution_y: 800,
    };
    let world = World{
        //background: Color{gray:1.},
        bricks: vec![b1, b2, b3, b4, b5, b6],
        n: 1.,
        max_reflections: 8,
    };
    render(&world, &camera);
    println!("Hello, world!");
}

extern crate test;
use test::Bencher;

#[test]
fn it_works() {
    assert_eq!(4, 4);
}

#[bench]
fn bench_dotvector3d(b: &mut Bencher) {
    let mut v = Vector3d::new(4.,4.,4.);
    let face = &FACES[0];
    b.iter(|| {
        //let mut dsum = 0.;
        for i in 0..1000 {
            let d = face.dot_vector(v);
            //dsum = dsum + d;
            //v.y = v.y+0.1*d;
        }
    });
}

#[bench]
fn bench_intersectface(b: &mut Bencher) {
    let p0 = Position::new(0,0,1);
    let ray = Ray{origin: Vector3d::new(0.5,0.5,0.),
                  direction: UnitVector3d::new(0.01, 0.01, 1.),
                  reflections: 0,
                  importance: 1.
                };
    let face = &FACES[0];
    b.iter(|| {
        //let mut dsum = 0.;
        for i in 0..1000 {
            let (d, p) = intersect_face(p0, face, &ray).unwrap();
            //dsum = dsum + d;
        }
    });
}

#[bench]
fn bench_reflect(b: &mut Bencher) {
    let v = UnitVector3d::new(0.01, 0.01, 1.);
    let face = &FACES[0];
    b.iter(|| {
        //let mut dsum = 0.;
        for i in 0..1000 {
            let u = v.reflect(face);
            //let (d, p) = intersect_face(p0, face, &ray).unwrap();
            //dsum = dsum + d;
        }
    });
}