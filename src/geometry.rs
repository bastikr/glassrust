use std::ops::{Add,Sub,Neg};
use std::cmp::PartialEq;

struct BasisVector {
    pub index: i64,
    pub positive: bool,
}

impl BasisVector {
    fn dot_vector(&self, v: Vector3d) -> f64 {
        if self.positive {
            match self.index {
                1 => v.x,
                2 => v.y,
                3 => v.z,
                _ => unreachable!(),
            }
        } else {
            match self.index {
                1 => -v.x,
                2 => -v.y,
                3 => -v.z,
                _ => unreachable!(),
            }
        }
    }

    fn dot_unitvector(&self, v: UnitVector3d) -> f64 {
        if self.positive {
            match self.index {
                1 => v.x,
                2 => v.y,
                3 => v.z,
                _ => unreachable!(),
            }
        } else {
            match self.index {
                1 => -v.x,
                2 => -v.y,
                3 => -v.z,
                _ => unreachable!(),
            }
        }
    }

    fn scale(&self, s: f64) -> Vector3d {
        if self.positive {
            match self.index {
                1 => Vector3d::new(s, 0., 0.),
                2 => Vector3d::new(0., s, 0.),
                3 => Vector3d::new(0., 0., s),
                _ => unreachable!(),
            }
        } else {
            match self.index {
                1 => Vector3d::new(-s, 0., 0.),
                2 => Vector3d::new(0., -s, 0.),
                3 => Vector3d::new(0., 0., -s),
                _ => unreachable!(),
            }
        }
    }
}



impl Neg for BasisVector {
    type Output = BasisVector;

    fn neg(self) -> BasisVector {
        BasisVector {index: self.index, positive: !self.positive}
    }
}


static ORIGIN0: Position = Position {x: 0, y: 0, z: 0};
static ORIGIN1: Position = Position {x: 1, y: 1, z: 1};
static EX: BasisVector = BasisVector {index: 1, positive: true};
static EY: BasisVector = BasisVector {index: 2, positive: true};
static EZ: BasisVector = BasisVector {index: 3, positive: true};
static MEX: BasisVector = BasisVector {index: 1, positive: false};
static MEY: BasisVector = BasisVector {index: 2, positive: false};
static MEZ: BasisVector = BasisVector {index: 3, positive: false};

pub struct Face {
    origin: &'static Position,
    normal: &'static BasisVector,
    e1: &'static BasisVector,
    e2: &'static BasisVector,
}

impl PartialEq for Face {
    fn eq(&self, other: &Face) -> bool {
        (self.normal.positive==other.normal.positive) && (self.normal.index==other.normal.index)
    }
}

impl Face {
    pub fn dot_vector(&self, v: Vector3d) -> f64 {
        self.normal.dot_vector(v)
    }

    pub fn dot_unitvector(&self, v: UnitVector3d) -> f64 {
        self.normal.dot_unitvector(v)
    }

    pub fn isinside(&self, p: Vector3d) -> bool {
        let f1 = self.e1.dot_vector(p);
        let f2 = self.e2.dot_vector(p);
        0.<=f1 && f1<=1. && 0.<=f2 && f2 <= 1.
    }

}


pub static FACES: [Face; 6] = [
    Face{origin:&ORIGIN0, normal:&MEZ, e1: &EX, e2: &EY},
    Face{origin:&ORIGIN0, normal:&MEX, e1: &EY, e2: &EZ},
    Face{origin:&ORIGIN0, normal:&MEY, e1: &EZ, e2: &EX},
    Face{origin:&ORIGIN1, normal:&EZ, e1: &MEX, e2: &MEY},
    Face{origin:&ORIGIN1, normal:&EX, e1: &MEY, e2: &MEZ},
    Face{origin:&ORIGIN1, normal:&EY, e1: &MEZ, e2: &MEX},
];


#[derive(Clone, Copy)]
pub struct Position {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

impl Add<Position> for Position {
    type Output = Position;

    fn add(self, other: Position) -> Position {
        return Position::new(self.x+other.x, self.y+other.y, self.z+other.z)
    }
}

impl PartialEq for Position {
    fn eq(&self, other: &Position) -> bool {
        self.x==other.x && self.y==other.y && self.z==other.z
    }
}

impl Position {
    pub fn new(x: i64, y: i64, z: i64) -> Position {
        Position{x: x, y: y, z: z}
    }

    pub fn vector3d(&self) -> Vector3d {
        Vector3d{x: self.x as f64, y: self.y as f64, z: self.z as f64}
    }
}

// fn adjacent_position(p: Position, face: &Face) -> Position {
//     if face.normal.positive {
//         match face.normal.index {
//             1 => Position {x: p.x+1, .. p},
//             2 => Position {y: p.y+1, .. p},
//             3 => Position {z: p.z+1, .. p},
//             _ => unreachable!(),
//         }
//     } else {
//         match face.normal.index {
//             1 => Position {x: p.x-1, .. p},
//             2 => Position {y: p.y-1, .. p},
//             3 => Position {z: p.z-1, .. p},
//             _ => unreachable!(),
//         }
//     }
// }


#[derive(Clone, Copy)]
pub struct Vector3d {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector3d {
    pub fn new(x: f64, y: f64, z: f64) -> Vector3d {
        Vector3d{x: x, y: y, z: z}
    }

    pub fn unitvector(&self) -> UnitVector3d {
        UnitVector3d::new(self.x, self.y, self.z)
    }
}

impl Add<Vector3d> for Vector3d {
    type Output = Vector3d;

    fn add(self, other: Vector3d) -> Vector3d {
        return Vector3d::new(self.x+other.x, self.y+other.y, self.z+other.z)
    }
}

impl Sub<Vector3d> for Vector3d {
    type Output = Vector3d;

    fn sub(self, other: Vector3d) -> Vector3d {
        return Vector3d::new(self.x-other.x, self.y-other.y, self.z-other.z)
    }
}



#[derive(Clone, Copy)]
pub struct UnitVector3d {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl UnitVector3d {
    pub fn new(x: f64, y: f64, z: f64) -> UnitVector3d {
        let n = (x.powi(2) + y.powi(2) + z.powi(2)).sqrt();
        UnitVector3d {x: x/n, y: y/n, z: z/n}
    }

    pub fn vector3d(&self) -> Vector3d {
        Vector3d{x: self.x, y: self.y, z: self.z}
    }

    pub fn scale(&self, factor: f64) -> Vector3d {
        Vector3d{x: self.x*factor, y: self.y*factor, z: self.z*factor}
    }

    pub fn reflect(&self, face: &Face) -> UnitVector3d {
        (self.vector3d() - face.normal.scale(2.*face.normal.dot_unitvector(*self))).unitvector()
    }

    pub fn cross(&self, b: UnitVector3d) -> UnitVector3d {
        UnitVector3d::new(
            self.y*b.z - self.z*b.y,
            self.z*b.x - self.x*b.z,
            self.x*b.y - self.y*b.x)
    }
}



pub struct Ray {
    pub origin: Vector3d,
    pub direction: UnitVector3d,
    pub reflections: usize,
    pub importance: f64,
}


pub fn intersect_face(p0: Position, face: &Face, ray: &Ray) -> Option<(f64,Vector3d)> {
    let p_face = (p0 + *face.origin).vector3d();
    let d = face.dot_vector(p_face - ray.origin)/face.dot_unitvector(ray.direction);
    if d < 0. {return None}
    let x = ray.origin + ray.direction.scale(d);
    if face.isinside(x-p_face) {
        return Some((d, x))
    }
    None
}

